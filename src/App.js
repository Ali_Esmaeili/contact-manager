
import { useState } from 'react';
import './App.css';
import {Navbar, Contacts} from './components';

const App =()=> {
  const [loading , setloading] = useState(false);
  const [getContact,setContact]= useState([]);
  return (
    <div className="App">
     <Navbar/>
     <Contacts contact={getContact} loading={loading}/>
      </div>
  );
}

export default App;
