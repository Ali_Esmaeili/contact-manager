import {  BACKGROUND, GREEN, LIGHTORANGE } from "../../helpers/colors";
import Contact from "./Contact";
import NotFound from "../../assets/no-found.gif";
import Spinner from "../Spinner";
const Contacts = ({contact , loading}) => {
    return (  
    <>
    <section className="container"  style={{marginTop:"5px"}}>
        <div className="grid">
            <div className="row">
                <div className="col">
                    <p className="h3">
                        <button className="btn mx-2" 
                        style={{backgroundColor:GREEN}}>
                            ساخت مخاطب جدید 
                            <i className="fa fa-plus-circle mx-2"/>
                        </button>
                    </p>
                </div>
            </div>
        </div>
    </section>
    {
        loading ? <Spinner/> :(
        <section className="container">
        <div className="row">
{
    contact.length >0 ? contact.map(c =>(
        <Contact key={c.id} contact={c}/>
    )):
    <div className="text-center py-5" style={{backgroundColor:BACKGROUND}}>
        <p className="h3" style={{color:LIGHTORANGE}}>
            مخاطب یافت نشد !!!
        </p>
        <img src={NotFound} alt="تصویر یافت نشد " className="w-25" />
    </div>
}        </div>
    </section>)
    }
   
    </>
    );
}
 
export default Contacts;
