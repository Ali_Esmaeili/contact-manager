import { BACKGROUND, LIGHTBLUE, LIGHTORANGE, LIGHTPINK, LIGHTPURPUL } from "../../helpers/colors";

const Contact = () => {
    return ( 
        <div className="col-md-6">
            <div className="card my-2"
            style={{backgroundColor : BACKGROUND}}>
             <div className="card-body">
                 <div className="row align-items-center d-flex justify-content-around">
                     <div className="col-md-4 col-sm-4">
                        <img src="https://via.placeholder.com/200" alt="تصویر یافت نشد"
                        style={{border:`1px solid ${LIGHTPURPUL}`}}
                        className="img-fluid rounded"/>
                     </div>
                     <div className="col-md-7 col-sm-7">
                        <ul className="list-group">
                            <li className="list-group-item list-group-item-dark">
                                نام و نام خانوادگی : {" "}
                                <span className="fw-bold">
                                    یک مشت متن آماده
                                </span>
                            </li>
                            <li className="list-group-item list-group-item-dark">
                                شماره تماس : {" "}
                                <span className="fw-bold">
                                    یک مشت متن آماده
                                </span>
                            </li>
                            <li className="list-group-item list-group-item-dark">
                                آدرس ایمیل : {" "}
                                <span className="fw-bold">
                                    یک مشت متن آماده
                                </span>
                            </li>
                        </ul>
                     </div>
                     <div className="col-md-1 col-sm-1 d-flex flex-column align-items-center">
                         <button className="btn my-1" style={{backgroundColor:LIGHTORANGE}}>
                             <i className="fa fa-eye" />
                         </button>
                         <button className="btn my-1" style={{backgroundColor:LIGHTBLUE}}>
                             <i className="fa fa-pen" />
                         </button>
                         <button className="btn my-1" style={{backgroundColor:LIGHTPINK}}>
                             <i className="fa fa-trash" />
                         </button>
                     </div>
                 </div>
             </div>
           </div> 
        </div> );
}
 
export default Contact;