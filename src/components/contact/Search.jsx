import {  LIGHTGRAY, LIGHTPURPUL } from "../../helpers/colors";

const Search = () => {
    return ( 
        <div className="input-group mx-2 w-75" dir="ltr">
                              <span className="input-group-text" id="basic-addon1" style={{backgroundColor:LIGHTPURPUL}}>
                                  <i className="fas fa-search" 
                                  style={{ borderColor:LIGHTPURPUL , cursor:"pointer"}}
                                  />
                              </span>
                              <input 
                              dir="rtl" 
                              type="text" 
                              style={{backgroundColor:"white" , borderColor:LIGHTPURPUL}}
                              className="form-control"
                              placeholder="جستجو مخاطبین"
                              aria-label="Search"
                              aria-describedby="basic-addon1"
                              />
                          </div>
     );
}
 
export default Search;