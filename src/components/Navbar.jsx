import Search from "./contact/Search";
import {BACKGROUND,LIGHTPURPUL} from '../helpers/colors'
const Navbar = () => {
    return ( 
        <>
        <nav className="navbar navbar-dark expand-sn shadow-lg" style={{backgroundColor:BACKGROUND}}>
            <div className="container">
                <div className="row w-100">
                    <div className="col">
                        <div className="navbar-brand">
                        <i className="fas fa-id-badge "
                        
                        />{"  "}
                        وب اپلیکیشن مدیریت {" "}
                        <span style={{color:LIGHTPURPUL}}>مخاطبین</span>
                     
                        </div>
                       
                    </div>
                      <div className="col">
                          <Search/>
                      </div>
                </div>
            </div>
        </nav>
        </>
     );
}
 
export default Navbar;