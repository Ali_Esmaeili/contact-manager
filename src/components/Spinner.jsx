import SpinnerGif from '../assets/Dual-Ring-Preloader-1.gif';
const Spinner = () => {
    return ( 
    <>
    <img src={SpinnerGif} alt="یافت نشد " className='w-200 m-auto d-block ' />
    </>
     );
}
 
export default Spinner;